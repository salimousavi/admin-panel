import {combineReducers} from 'redux'
import {persons, person} from './person'
import {posts, post} from './post'
import {user, isLoggedIn, loginSubmitLoading} from './user'

export default combineReducers({
  persons,
  person,
  posts,
  post,
  user,
  isLoggedIn,
  loginSubmitLoading
})