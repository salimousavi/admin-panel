import request from '../../tools/Request'
import { createAction } from '@reduxjs/toolkit'

const posts = createAction('POSTS')
const post = createAction('POST')

export const getItems = () => dispatch => request('/posts').then(response => dispatch(posts(response.data)))

export const getItem = id => dispatch =>
  request({url: `/posts/${id}`, actionType: 'POST'})
    .then(response => dispatch(post(response.data)))
