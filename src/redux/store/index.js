// import {createStore, applyMiddleware} from 'redux'
import {configureStore} from '@reduxjs/toolkit'
// import thunk from 'redux-thunk'
import reducers from '../reducers'


// const myMiddleware = function (store) {
//   return function (next) {
//     return function (action) {
//       console.log("store ==> ", store)
//       console.log("next ==> ", next)
//       console.log("action ==> ", action)
//
//       next(action)
//     }
//   }
// }

// const myMiddleware = store => next => action => {
//   console.log("store ==> ", store)
//   console.log("next ==> ", next)
//   console.log("action ==> ", action)
//   console.log("type of action ==> ", typeof action)
//
//   if (typeof action === 'function') {
//     return action(store.dispatch)
//   }
//
//   setTimeout(() => next(action), 1000)
//   next(action)
//
// }
// const logMiddleware = store =>  next => action => {
//   console.log("LOG MIDDLEWARE =>", action)
//   next(action)
// }

// const store = createStore(reducers, applyMiddleware(thunk))
const store = configureStore({
  reducer: reducers,
})

export default store