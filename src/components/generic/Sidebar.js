import React from 'react'
import {Link} from 'react-router-dom'
import { Menu } from 'antd'
import { AppstoreOutlined, MailOutlined, SettingOutlined, HomeOutlined } from '@ant-design/icons'

const { SubMenu } = Menu

class Sidebar extends React.Component {
  handleClick = e => {
    console.log('click ', e)
  }

  render() {
    return (
      <Menu
        onClick={this.handleClick}
        defaultSelectedKeys={['/']}
        defaultOpenKeys={['sub1']}
        mode="inline"
      >
        <Menu.Item key={'/'}>
          <HomeOutlined />
          <Link to="/">
            داشبورد
          </Link>
        </Menu.Item>
        <SubMenu
          key="sub1"
          title={
            <span>
              <MailOutlined />
              <span>مدیریت موجودیت‌ها</span>
            </span>
          }
        >
          <Menu.ItemGroup key="g1" title="کاربران">
            <Menu.Item key="1">
              <Link to="/person">
                لیست کاربران
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/person/new" >افزودن کاربر</Link>
            </Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup key="posts" title="مقالات">
            <Menu.Item key="posts-list">
              <Link to="/post">
                لیست مقالات
              </Link>
            </Menu.Item>
            <Menu.Item key="post-full">
              <Link to="/post/new">افزودن مقاله</Link>
            </Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup key="g2" title="Item 2">
            <Menu.Item key="3">Option 3</Menu.Item>
            <Menu.Item key="4">Option 4</Menu.Item>
          </Menu.ItemGroup>
        </SubMenu>
        <SubMenu key="sub2" icon={<AppstoreOutlined />} title="Navigation Two">
          <Menu.Item key="5">Option 5</Menu.Item>
          <Menu.Item key="6">Option 6</Menu.Item>
          <SubMenu key="sub3" title="Submenu">
            <Menu.Item key="7">Option 7</Menu.Item>
            <Menu.Item key="8">Option 8</Menu.Item>
          </SubMenu>
        </SubMenu>
        <SubMenu
          key="sub4"
          title={
            <span>
              <SettingOutlined />
              <span>Navigation Three</span>
            </span>
          }
        >
          <Menu.Item key="9">Option 9</Menu.Item>
          <Menu.Item key="10">Option 10</Menu.Item>
          <Menu.Item key="11">Option 11</Menu.Item>
          <Menu.Item key="12">Option 12</Menu.Item>
        </SubMenu>
      </Menu>
    )
  }
}

export default Sidebar