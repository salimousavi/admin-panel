import React from 'react'
import { Form, Input, Button, Checkbox, Row, Col, Divider } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import {login} from '../../redux/actions/user'

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

function Login () {
  const dispatch = useDispatch()
  const loginSubmitLoading = useSelector(state => state.loginSubmitLoading)

  const onFinish = (values) => {
    console.log('Success:', values);
    dispatch(login(values))
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Row justify="space-around" align="middle" style={{marginTop: '150px'}}>
      <Col span={8}>
        <Divider>ورود به پنل ادمین</Divider>
        <Form
          {...layout}
          name="login"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="نام‌کاربری"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input/>
          </Form.Item>

          <Form.Item
            label="رمزعبور"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password/>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit" loading={loginSubmitLoading} disabled={loginSubmitLoading}>
              ورود
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
}

export default Login

