import React from 'react'
import { Button, Form, Input } from 'antd'

export function Text({label, name, required, initialValue}) {
  return (
    <Form.Item
      label={label}
      name={name}
      initialValue={initialValue}
      rules={[
        {
          required,
          message: `فیلد ${label} اجباری می‌باشد`,
        },
      ]}
    >
      <Input />
    </Form.Item>
  )
}

export function TextArea({label, name, required, initialValue}) {
  return (
    <Form.Item
      label={label}
      name={name}
      initialValue={initialValue}
      rules={[
        {
          required,
          message: `فیلد ${label} اجباری می‌باشد`,
        },
      ]}
    >
      <Input.TextArea />
    </Form.Item>
  )
}

export function Submit({label}) {
  return (
    <Form.Item>
      <Button type="primary" htmlType="submit">
        {label}
      </Button>
    </Form.Item>
  )
}