import React from 'react'
import { connect } from 'react-redux'
import { Table, Typography } from 'antd';
import { EyeOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'
import {persons} from '../../redux/actions/person'

const {Paragraph} = Typography

class List extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      // data: [],
      loading: true
    }
  }

  inlineUpdate (value, id) {
    console.log(value, id)
  }

  columns = [
    {
      title: 'نام',
      dataIndex: 'name',
      key: 'name',
      render: (field, record, i) => <Paragraph
        editable={{onChange: (value) => this.inlineUpdate(value, record.id)}}>{field}</Paragraph>
    },
    {
      title: 'نام کاربری',
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: 'آدرس',
      dataIndex: 'address',
      key: 'address',
      render: field => `${field.city} ${field.street} ${field.suite}`
    },
    {
      title: '',
      dataIndex: 'actions',
      key: 'actions',
      render: (field, record) => <Link to={`/person/${record.id}/show`}><EyeOutlined/></Link>
    }
  ];

  componentDidMount () {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      // .then(data => this.setState({data}))
      .then(data => this.props.setItems(data))
      .catch(error => console.log(error))
      .finally(() => this.setState({loading: false}))

  }

  render () {
    return (
      <div>
        <Table columns={this.columns} dataSource={this.props.data} rowKey="id" loading={this.state.loading}/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.persons
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setItems: data => dispatch(persons(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)










