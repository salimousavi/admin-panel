import React, { useEffect } from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { Link, useParams } from 'react-router-dom'
import axios from 'axios'
import {Row, Col} from 'antd'
import {person} from '../../redux/actions/person'

function Full () {

  const {id} = useParams()

  const dispatch = useDispatch()
  // const state = useSelector(state => state)
  const item = useSelector(state => state.person)

  // console.log(state)
  // const item = state.person
  useEffect(() => {
    dispatch(person({}))
    axios(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(response => dispatch(person(response.data))) //{type: 'PERSON', payload: {...}}
  }, [id])

  return (
    <div>
      <div>
        <Row>
          <Col span={4}>نام:</Col>
          <Col span={20}>{item.name}</Col>
        </Row>
        <Row>
          <Col span={4}>نام‌کاربری:</Col>
          <Col span={20}>{item.username}</Col>
        </Row>
        <Row>
          <Col span={4}>ایمیل:</Col>
          <Col span={20}>{item.email}</Col>
        </Row>
      </div>

      <hr/>
      <Link to="/person">بازگشت به لیست</Link>
    </div>
  )
}

export default Full