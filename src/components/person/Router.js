import React from 'react'
import { Switch, Route } from 'react-router-dom'
import List from './List'
import Full from './Full'
import New from './New'

function Router () {
  return <>
    <Switch>
      <Route exact path="/person" component={List}/>
      <Route exact path="/person/new" component={New}/>
      <Route exact path="/person/:id/show" component={Full}/>
    </Switch>
  </>
}

export default Router