import React from 'react'
import {Form, Input, Button} from 'antd'
import {Text, Submit, TextArea} from '../utils/Field'

function New() {

  function onFinish(valuse) {
    console.log(valuse)
  }

  return (
    <Form onFinish={onFinish}>
      <Text label="عنوان" name="title" required={true} initialValue={"سلام"}/>

      <Text label="خلاصه" name="desc"/>

      <TextArea label="توضیحات" name={'intro'}/>

      <Submit label="ثبت" />

    </Form>
  )
}

export default New