import React, { useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import { Layout } from 'antd'
import Sidebar from './generic/Sidebar'
import Header from './generic/Header'
import Footer from './generic/Footer'
import Dashboard from './generic/Dashboard'
import Login from './generic/Login'
import PersonRouter from './person/Router'
import PostRouter from './post/Router'
import 'antd/dist/antd.css'
import '../assets/css/general.css'
import NotFound from './generic/NotFound'
import { useDispatch, useSelector } from 'react-redux'
import {getUser} from '../redux/actions/user'

const {Header: AntHeader, Footer: AntFooter, Sider, Content} = Layout

function App () {

  const isLoggedIn = useSelector(state => state.isLoggedIn)
  const dispatch = useDispatch()

  useEffect(() => { dispatch(getUser()) }, [])

  // if (!isLoggedIn) {
  //   return <Login />
  // }

  return (
    <Layout>

      <AntHeader>
        <Header/>
      </AntHeader>

      <Layout>

        <Sider>
          <Sidebar/>
        </Sider>

        <Content style={{padding: '50px'}}>
          <Switch>
            <Route exact path="/" component={Dashboard}/>
            <Route exact path="/person*" component={PersonRouter}/>
            <Route exact path="/post*" component={PostRouter}/>
            <Route path="*" component={NotFound}/>
          </Switch>
        </Content>

      </Layout>

      <AntFooter>
        <Footer/>
      </AntFooter>
    </Layout>
  )
}

export default App
