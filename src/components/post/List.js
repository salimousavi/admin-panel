import React from 'react'
import {connect} from 'react-redux'
import { Table } from 'antd'
import { Link } from 'react-router-dom'
import { EyeOutlined } from '@ant-design/icons'
import {posts, getItems} from '../../redux/actions/post'

const columns = [
  {
    title: 'عنوان',
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: '',
    dataIndex: 'actions',
    key: 'actions',
    render: (field, record) => <Link to={`/post/${record.id}/show`}><EyeOutlined /></Link>
  }
];

class List extends React.Component {

  componentDidMount () {
    this.props.getData()
  }

  render () {
    return (
      <Table columns={columns} dataSource={this.props.items} rowKey="id"/>
    );
  }
}

const mapStateToProps = state => ({
  items: state.posts
})

const mapDispatchToProps = dispatch => {
  return {
    getData: () => dispatch(getItems())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)