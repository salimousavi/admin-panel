import React, { useEffect } from 'react'
import { useParams } from 'react-router'
import { useDispatch, useSelector } from 'react-redux'
import {Row, Col} from 'antd'
import {getItem} from '../../redux/actions/post'
import { Link } from 'react-router-dom'

function useFetch(cb, id) {
  const dispatch = useDispatch()

  useEffect(() => { dispatch(cb(id))}, [id])
}

function Full() {

  const {id} = useParams()
  // const dispatch = useDispatch()
  const item = useSelector(state => state.post)

  // useEffect(() => { dispatch(getItem(id))}, [id])

  useFetch(getItem, id)

  return (
    <div>
      <Row>
        <Col span={4}>عنوان</Col>
        <Col span={20}>{item.title}</Col>
      </Row>
      <Row>
        <Link to="/post" >بازگشت به لیست</Link>
      </Row>
    </div>
  )
}

export default Full