import React from 'react'
import { Switch, Route } from 'react-router-dom'
import List from './List'
import Full from './Full'
import New from './New'

function Router () {
  return <>
    <Switch>
      <Route exact path="/post" component={List}/>
      <Route exact path="/post/new" component={New}/>
      <Route exact path="/post/:id/show" component={Full}/>
    </Switch>
  </>
}

export default Router