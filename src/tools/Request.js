import axios from 'axios'
import {BASE_URL} from './Constants'

axios.defaults.headers.common['Content-Type'] = 'application/json';
// axios.defaults.headers.common['Authorization'] = getAuthToken();
axios.defaults.baseURL = BASE_URL

const requestInstance = axios.create()

function showMessage(error) {

}

const user_python = {
  first_name: 'SAli',
  last_name: 'Mousavi'
}
const userJavaScript = {
  firstName: 'SAli',
  lastName: 'Mousavi'
}

requestInstance.interceptors.request.use(function (config) {
  console.log("config ===> ",config)
  // config.data = convertToSnackCase(config.data)
  // convertToSnackCase
  return config;
}, function (error) {
  showMessage(error)
  return Promise.reject(error);
})

requestInstance.interceptors.response.use(function (response) {
  console.log("response ====> ", response)
  // convertToCamelCase
  return response;
}, function (error) {
  showMessage(error)
  return Promise.reject(error);
})

export default requestInstance
